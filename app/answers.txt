
Question:  Input your Name, Age, Gender s display this information in another activity.

AndroidManifest.xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.texas">

  <application
     .....
      android:label="Demo App"
     ......

   />

    <activity android:name=".FirstActivity"
            android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
     </activity>

   <activity android:name=".SecondActivity"/>

</manifest>


activity_first.xml

<?xml version="1.0" encoding="utf-8"?>
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:padding="16dp"
    android:orientation="vertical"
    android:layout_height="match_parent">

    <EditText
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/et_name"
        />

    <EditText
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/et_address"
        />

    <RadioGroup
        android:layout_width="wrap_content"
        android:id="@+id/gender_group"
        android:layout_height="wrap_content" >

        <RadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Male"
            android:id="@+id/male"
            />

        <RadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Female"
            android:id="@+id/female"
            />

    </RadioGroup>


    <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:id="@+id/btn_submit"
        android:text="Submit"
        />

</LinearLayout>






FirstActivity.java


package com.example.texas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class FirstActivity extends AppCompatActivity {
    private EditText nameEditText;
    private EditText addressEditText;
    private RadioButton maleRadioButton;
    private Button submitButton;
    private RadioGroup genderGroup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        nameEditText = findViewById(R.id.et_name);
        addressEditText = findViewById(R.id.et_address);
        submitButton = findViewById(R.id.btn_submit);
        genderGroup = findViewById(R.id.gender_group);
        maleRadioButton = findViewById(R.id.male);
        submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                int id =  genderGroup.getCheckedRadioButtonId();
                if(id <= 0){
                    Toast.makeText(FirstActivity.this, "Gender is required...", Toast.LENGTH_SHORT).show();
                }else{
                    String gender = maleRadioButton.isChecked() ? "Male" : "Female";
                    String name = nameEditText.getText().toString();
                    String address = addressEditText.getText().toString();
                    Intent intent = new Intent(FirstActivity.this,SecondActivity.class);
                    intent.putExtra("Name",name);
                    intent.putExtra("Address",address);
                    intent.putExtra("Gender",gender);
                    startActivity(intent);
                }
            }
        });
    }

}


activity_second.xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:padding="16dp"
    android:orientation="vertical"
    android:layout_height="match_parent">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="10dp"
        android:id="@+id/tv_name"
        />

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="10dp"
        android:id="@+id/tv_address"
        />


    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="10dp"
        android:id="@+id/tv_gender"
        />

</LinearLayout>


SecondActivity.java
public class SecondActivity extends AppCompatActivity {
    private TextView nameTextView;
    private TextView addressTextView;
    private TextView genderTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        nameTextView = findViewById(R.id.tv_name);
        addressTextView = findViewById(R.id.tv_address);
        genderTextView = findViewById(R.id.gender);

        String name = getIntent().getStringExtra("Name");
        String address = getIntent().getStringExtra("Address");
        String gender = getIntent().getStringExtra("Gender");

        nameTextView.setText("Name: "+ name);
        addressTextView.setText("Address: "+ address);
        genderTextView.setText("Gender: "+ gender);
    }

}




Question::: Please try to display the information in dialog fragment too.....


Question:: Android Architecture


Android Architecture
Software stack of components to support mobile device needs. Android software stack contains a Linux Kernel, collection of c/c++ libraries which are exposed through an application framework services, runtime, and application.
1.Application:  Top layer, Contains native or third party apps → contacts, email, music, gallery, Texas, etc. whatever we will build those will be installed on this layer only


2. Application Framework:
- provides the classes used to create Android applications
- provides a generic abstraction for hardware access and manages the user interface and application resources
- telephony service, location services, notification manager, NFC service, view system, etc. which we can use for application    development as per our requirements.


3. Android Runtime Environment:
- Important part of android rather than an internal part
- contains core libraries and the Dalvik virtual machine(DVM).
- engine that powers our applications along with the libraries and it forms the basis for the application framework.
- DVM is a register-based virtual machine like Java Virtual Machine (JVM). It is specially designed and optimized for android to ensure that a device can run multiple instances efficiently. It relies on the Linux kernel for threading and low-level memory management.
- The core libraries in android runtime will enable us to implement android applications using standard JAVA programming language


4. Platform Libraries:
-Includes various C/C++ core libraries and Java-based libraries such as SSL(internet security), libc, Graphics, SQLite(database), Webkit(web browser support), Media(dealing with audio/video), Surface Manger(display management), SGL & OpenGL(2D & 3D graphics), FreeType(font support) etc. to provide support for Android development.

5. Linux Kernel

   - Bottom layer and heart of the android architecture.
   - It manages all the drivers such as display drivers, camera drivers,bluetooth drivers, audio drivers, memory drivers, etc.(needs on runtime)
   - Provide an abstraction layer between the device hardware and the remainder of the stack.
   - memory management, power management, device management, resource access, etc.




---> For Events and please provide example too and short description

onClick() → View.OnClickListener
onLongClick()  → View.OnLongClickListener
onMenuClickItem() → OnMenuItemClickListener
onFocusChange() → View.OnFocusChangeListener
onCheckedChanged() → OnCheckedChangeListener
onCreateContextMenu() → View.OnCreateContextMenuListener


