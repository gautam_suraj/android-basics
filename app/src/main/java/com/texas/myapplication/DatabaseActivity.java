package com.texas.myapplication;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.texas.myapplication.database.DatabaseHandler;
import com.texas.myapplication.model.Student;
import com.texas.myapplication.R;

import java.util.List;

public class DatabaseActivity extends AppCompatActivity {
    private String TAG = DatabaseHandler.class.getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        Log.d(TAG, "inserting Data "  );

        //inserting students
        databaseHandler.addStudent(new Student("Ram","Kanchanpur","4"));
        databaseHandler.addStudent(new Student("Shyam","Kathmandu","6"));
        databaseHandler.addStudent(new Student("Hari","Bardiya","3"));
        databaseHandler.addStudent(new Student("Laxam","Jhapa","7"));

        //reading all students
        Log.d(TAG, "reading/getting students: ");
        List<Student> students = databaseHandler.getStudents();

        /*
        * show in adapter
        *
        * */

    }


}
