package com.texas.myapplication;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.texas.myapplication.R;

public class DemoGoogleMap extends AppCompatActivity implements OnMapReadyCallback {


    /*
    *  1. https://console.cloud.google.com/projects
    *  2. Create new project
    *  3. Click Api and Services
    *  4. Enable Api and Services
    *  5. Credentials
    *  6. Create Credentials
    *
    * */

    /*
    * manifest -->
    *   permission -->  ACCESS_FINE_LOCATION
    *
    *
    * meta-data --> your api key
    *
    * */

    GoogleMap map;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng latLng = new LatLng(26.644096, 87.989391);
        map.addMarker(new MarkerOptions().position(latLng).title("Texas"));
        //enable to move
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //zoom effect
        map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
    }
}
