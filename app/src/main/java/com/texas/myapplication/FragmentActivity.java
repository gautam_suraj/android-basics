package com.texas.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.texas.myapplication.fragment.SampleFragment;
import com.texas.myapplication.model.Student;
import com.texas.myapplication.R;

public class FragmentActivity extends AppCompatActivity {
    public static final String TAG = FragmentActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        Intent intent = getIntent();
//        String name = intent.getStringExtra("name");
//        Log.d("FragmentActivity", "onCreate: " + name);


        Student student = (Student) getIntent().getSerializableExtra("student");
        Log.d(TAG, "onCreate: " + student);

        SampleFragment fragment = new SampleFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("student",student);
        fragment.setArguments(bundle);


        /*
        * FragmentManager is the class responsible for performing actions on your app's fragments,
        *  such as adding, removing, or replacing them, and adding them to the back stack.
        *
        * */
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.fragment, fragment);
        // or ft.add(R.id.your_placeholder, new FooFragment());
        // Complete the changes added above
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        /*
        * sending this data to calling/parent fragment
        *
        * */

        Intent intent = new Intent();
        intent.putExtra("name","Ram");
        setResult(Activity.RESULT_OK,intent);

        //setResult(Activity.RESULT_CANCELED);



        finish(); // we are killing/destroying this activity
    }
}
