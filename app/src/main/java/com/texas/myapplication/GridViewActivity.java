package com.texas.myapplication;

import android.os.Bundle;
import android.widget.GridView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.texas.myapplication.adapter.GridViewAdapter;
import com.texas.myapplication.model.GridItem;
import com.texas.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {

    /*
    *
    * A GridView is a type of AdapterView that displays items in a two-dimensional scrolling grid.
    *  Items are inserted into this grid layout from a database or from an array.
    * The adapter is used for displaying this data, setAdapter() method is used to join the adapter with GridView.
    * The main function of the adapter in GridView is to fetch data from a database or array
    * and insert each piece of data in an appropriate item that will be displayed in GridView.
    *
    *
    *
    * */

    /*
    * XML Attributes of GridView
        android:numColumns: This attribute of GridView will be used to decide the number of columns that are to be displayed in Grid.
        android:horizontalSpacing: This attribute is used to define the spacing between two columns of GridView.
        android:verticalSpacing: This attribute is used to specify the spacing between two rows of GridView.
    *
    * */
    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry",
            "WebOS","Ubuntu","Windows7","Max OS X"};
    List<GridItem> items = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        GridView listView = (GridView) findViewById(R.id.gridView);
        items.add(new GridItem("Alarm",R.drawable.d2));
        items.add(new GridItem("Account",R.drawable.d1));


        //SimpleArrayAdapter adapter = new SimpleArrayAdapter(this,R.layout.layout_simple_adapter_item,items);

        GridViewAdapter adapter = new GridViewAdapter(this,items);
        listView.setAdapter(adapter);
    }
}
