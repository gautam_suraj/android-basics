package com.texas.myapplication;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.texas.myapplication.adapter.SimpleArrayAdapter;
import com.texas.myapplication.model.ArrayItem;
import com.texas.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {
    /*
    * --> A ListView is a type of AdapterView that displays a vertical list of scroll-able views
    * --> AdapterView is a ViewGroup that displays items loaded into an adapter
    * --> Each view is placed one below the other.
    * --> Using adapter, items are inserted into the list from an array or database.
    * -->  For displaying the items in the list method setAdaptor() is used. setAdaptor() method conjoins an adapter with the list.

*     List
      * --> Android ListView is a ViewGroup that is used to display the list of items in multiple rows
      * --> contains an adapter that automatically inserts the items into the list.

     The main purpose of the adapter is to fetch data from an array or database
     and insert each item that placed into the list for the desired result.

    So, it is the main source to pull data from strings.xml file which contains all the required strings in Java or XML files.
    *
    *
    * */

    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry",
            "WebOS","Ubuntu","Windows7","Max OS X"};
    List<ArrayItem>  items = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        ListView listView = (ListView) findViewById(R.id.mobile_list);
        items.add(new ArrayItem("Title1","SubTitle1"));
        items.add(new ArrayItem("Title2","SubTitle2"));
        items.add(new ArrayItem("Title3","SubTitle3"));
        items.add(new ArrayItem("Title4","SubTitle4"));
        items.add(new ArrayItem("Title5","SubTitle5"));
        SimpleArrayAdapter adapter = new SimpleArrayAdapter(this,R.layout.layout_simple_adapter_item,items);

      /*  ArrayAdapter<String> adapter = new ArrayAdapter(this,
                R.layout.layout_text_item, mobileArray);*/

        listView.setAdapter(adapter);
    }

    /*
    * ListView has been around since API version 1 and is used to display a list of items that can be scrolled vertically.
    *
    * A ListView is an adapter view and uses a ListAdapter to create and populate the item Views.
    *
    * ArrayAdapter and SimpleCursorAdapter are examples of adapters than can be used to provide the items to the ListView
    * by using the setAdapter(ListAdapter) method and passing the Adapter to bind data the ListView.
    *
    * SimpleCursorAdapter can be used when you have data coming from a
    * Cursor such as results of a database query or content provider you want displayed in a ListView.
    *
    * limited optimization
    *
    *
    *
    *
    * */
}
