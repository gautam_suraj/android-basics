package com.texas.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.texas.myapplication.R;

/*

 * activity
 * service
 * receiver
 * content resolver
 *
 *
 * */

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    RadioButton male;
    EditText editText;
    RadioGroup gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_v2);
        TextView helloWorld = findViewById(R.id.hello_world);
        TextView kChha = findViewById(R.id.k_chha);
        Button submit = findViewById(R.id.btn_submit);
        gender = findViewById(R.id.gender);

        RadioButton female = findViewById(R.id.radio_female);
        male = findViewById(R.id.radio_male);
        editText = findViewById(R.id.edit_text);
        CharSequence helloWorldText = helloWorld.getText();
        String kChhaText = kChha.getText().toString();
        kChha.setText(helloWorldText);
        helloWorld.setText(kChhaText);

        kChha.setText(R.string.k_chha_in_english);
        kChha.setText("Hello how are you?");

        editText.setText("Ram Prasad Sharma");

        helloWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Hello world clicked",Toast.LENGTH_SHORT).show();
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Toast.makeText(MainActivity.this,charSequence.toString(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        editText.addTextChangedListener(textWatcher);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    Intent intent = new Intent(MainActivity.this,MultipleFragmentActivity.class);
                    startActivity(intent);
                }
            }

        });
    }

    private boolean validate(){
        String name = editText.getText().toString();
        if(name.isEmpty()){
            Toast.makeText(this,"Name is required..",Toast.LENGTH_SHORT).show();
            return false;
        }
        int id =  gender.getCheckedRadioButtonId();
        Log.d(TAG,"gender id "+ id);
        if(id <= 0){
            Toast.makeText(this, "Gender is required...", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            Toast.makeText(this,male.isChecked() ? "Male is selected.." : "Female is selected..", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}