package com.texas.myapplication;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.texas.myapplication.fragment.SampleDialogFragment;
import com.texas.myapplication.fragment.Fragment1;
import com.texas.myapplication.fragment.Fragment2;
import com.texas.myapplication.listener.DialogCallBackListener;
import com.texas.myapplication.R;

public class MultipleFragmentActivity extends AppCompatActivity implements DialogCallBackListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiplefragment);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_1,new Fragment1());
        ft.replace(R.id.fragment_2,new Fragment2());
        ft.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_multiple_fragment_activity,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.search :
                onSearchClicked();
                return true;

            case  R.id.settings:
                onSettingClicked();
                return true;

            case  R.id.change_language:
                onLanguageClicked();
                return true;

        }
        return false;
    }

    private void onSearchClicked(){
        showDialog();
    }

    private void onSettingClicked(){
        showDialogFragment();
    }

    private void onLanguageClicked(){

    }

    private void showDialog(){
        new AlertDialog.Builder(this)
                .setTitle("Show Dialog")
                .setMessage("We are showing alert dialog")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                      dialog.dismiss();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       //dialog.dismiss();
                    }
                })
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    private void showDialogFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        SampleDialogFragment dialogFragment = new SampleDialogFragment();
        dialogFragment.show(transaction,SampleDialogFragment.TAG);
    }

    @Override
    public void onDialogButtonClicked(){

    }



    //long press remaining


    /*
    * Activity                                                                                                                           Fragment

Activity is an application component that gives a user interface where the user can interact. 	        The fragment is only part of an activity, it basically contributes its UI to that activity.

Activity is not dependent on fragment 	                                                                Fragment is dependent on activity. It can’t exist independently.
 *
we need to mention all activity it in the manifest.xml file                                             Fragment is not required to mention in  the manifest file
*
We can’t create multi-screen UI without using fragment in an activity,	                                After using multiple fragments in a single activity, we can create a multi-screen UI.
*
Activity can exist without a Fragment  	                                                                Fragment cannot be used without an Activity.
*
Creating a project using only Activity then it’s difficult to manage	                                While Using fragments in the project, the project structure will be good and we can handle it easily.
*
Lifecycle methods are hosted by OS. The activity has its own life cycle.	                             Lifecycle methods in fragments are hosted by hosting the activity.
*
Activity is not lite weight. 	                                                                         The fragment is the lite weight.
    *
    *
    * */
}
