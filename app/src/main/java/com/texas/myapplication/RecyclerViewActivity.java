package com.texas.myapplication;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.texas.myapplication.adapter.RecyclerViewAdapter;
import com.texas.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity {
    /*
    * View Holder
    *
    * In a RecyclerView, your items will have a specific View, these Views are placed inside ViewHolders.
    * Each ViewHolder can have up to one View and  the ViewHolder is used to display a particular
    * View of an item within your RecyclerView.
    *
    *

        * For performance reasons,
        * the RecyclerView will reuse ViewHolders for displaying Views by recycling ViewHolders
        * that contain Views that shift offscreen with Views that shift onscreen when scrolling occurs.
*

       The RecyclerView Adapter contains methods for creating and binding the ViewHolder called onCreateViewHolder(…)
       * and onBindViewHolder(…) to enable this ViewHolder recycling to occur.
    *
    *
    *
    * */


    /*
    * Layout Manager
    * The RecyclerView uses a LayoutManager to determine how the Views will be placed within the RecyclerView.
    * The different LayoutManagers supported by RecyclerView include:

    1. LinearLayoutManager, a vertical or horizontal list of items
    2. GridLayoutManager, a grid of items that are all the same size
    3.StaggeredGridLayoutManager, a grid of items that supports different sizes
    *
    *
    * */

    /*
    * RecyclerView Adapter
    * ViewHolders are managed by RecyclerView adapters by creating the
    * Views and ViewHolders and binding the ViewHolder to an item in the list.
     Adapters notify the RecyclerView on specific changes to the data.
     * Adapters can also handle interactions with Items such as clicks.

     In addition to notifyDataSetChange(…) which is also supported by ListView
     * for advising on changes to the date in the adapter,
     * RecyclerView adapters also support the following methods:

        notifyItemInserted(…)
        notifyItemChanged(…)
        notifyItemRemoved(…)
        These methods available within RecyclerView (not supported in ListView)
        *  should be utilised over notifyDataSetChanged(…) where possible to enable RecyclerView animations to work correctly.
    *
    * */


    /*
    *
    * RecycledView Pool
    * RecycledViewPool lets you share Views between multiple RecyclerViews.
    * This comes in handy in scenarios where you have a nested RecyclerView and will provide a boost to your apps performance.
    *

        cUse the setRecycledViewPool(RecycledViewPool) method on the multiple RecyclerViews
        * providing the same instance of RecycledViewPool to share the pool.

         RecyclerView automatically creates a RecycledViewPool if you don’t provide one.
    *
    * */

    /*
    * Item Animator
    * Item Decoration
    *
    * */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        //setting layout manager to recycler view
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this); //default vertical
        recyclerView.setLayoutManager(linearLayoutManager);
        //setting adapter to recycler view
        List<String> items = new ArrayList<>();
        items.add("Science");
        items.add("Math");
        items.add("Social");
        items.add("Nepali");
        items.add("English");
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}


/*
*
* listview vs recycler view
*
*
*

Criteria	                                        RecyclerView	                                                            ListView
Requires additional dependency	 -->     Yes, RecyclerView requires an additional dependency             	No, available natively
*
ViewHolder Pattern -->	                Mandatory ViewHolder pattern providing better performance	         ViewHolder pattern can be supported with customisation
*
Layout Management -->	Advanced Layout Management capabilities for vertical and horizontal lists,
                                grids and staggered grids	                                                        Vertical list only


Adapters	--> Supports notifications if the entire data set has changed, item gets added,
*                        item gets removed or item gets changed	                                          Supports notifications if the entire data set has changed\
*
Item Decorations -->	Dividers between items not shown by default.
*                            Use ItemDecorations to add margin and draw on or under an Item View.	        Dividers between items shown by default. Item decoration requires customisation for ListView.
*
Item Animation --> 	                ItemAnimator makes it easy to add animations	                            Very complex to implement


*
* */
