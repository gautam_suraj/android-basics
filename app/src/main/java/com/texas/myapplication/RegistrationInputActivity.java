package com.texas.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.texas.myapplication.model.Student;
import com.texas.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class RegistrationInputActivity extends AppCompatActivity {
    private EditText name;
    private EditText address;
    private Button submit;
    private Spinner semester;
    private final String[] semesterList = { "First", "Second","Third","Forth","Fifth","Sixth","Seventh","Eight"};
    private CheckBox java;
    private CheckBox javaScript;
    private CheckBox python;
    private Student student;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        configureToolbar();
        name = findViewById(R.id.et_name);
        address = findViewById(R.id.et_address);
        submit = findViewById(R.id.btn_submit);
        semester = findViewById(R.id.semester);
        java = findViewById(R.id.cb_java);
        python = findViewById(R.id.cb_python);
        javaScript = findViewById(R.id.cb_javascript);
        student = new Student();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView nameRequired = findViewById(R.id.tv_name_required);

        ArrayAdapter adapter= new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                semesterList);

        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        semester.setAdapter(adapter);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name.getText().toString().isEmpty()){
                    nameRequired.setVisibility(View.VISIBLE);
                    return;
                }

                setSelectedItem();
                student.setAddress(address.getText().toString());
                student.setName(name.getText().toString());
                Intent intent = new Intent(RegistrationInputActivity.this,FragmentActivity.class);
                //intent.putExtra("name",student.getName());
                intent.putExtra("student",student);
                //startActivity(intent);
                startActivityForResult(intent,1234);

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 0){
                    nameRequired.setVisibility(View.GONE);
                }else{
                    nameRequired.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        semester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                student.setSemester(semesterList[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void configureToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setSelectedItem(){
        List<String> intrestedSubjects = new ArrayList<>();
        if(java.isChecked()){
            intrestedSubjects.add("Java");
        }
        if(javaScript.isChecked()){
            intrestedSubjects.add("JavaScript");
        }
        if(python.isChecked()){
            intrestedSubjects.add("Python");
        }
        student.setIntrestedSubject(intrestedSubjects);

    }




    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1234) {  //this result is from FragmentActivity
            if (resultCode == Activity.RESULT_OK) {  //work is successflly completed
               String name =  data.getStringExtra("name");
                Log.d("RegistrationActivity", "onActivityResult: " + name);
            } else if (resultCode == Activity.RESULT_CANCELED) { //work is not successflly completed

            }
        }
    }
}
