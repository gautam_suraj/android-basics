package com.texas.myapplication;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import com.texas.myapplication.R;

public class SecondActivity extends AppCompatActivity {

    private final String TAG = "Texas";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG , "onCreate");
        setContentView(R.layout.activity_main);
        TextView helloWorld = findViewById(R.id.hello_world);
        ImageView imageView = findViewById(R.id.image);

        //imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_alt_route_24,null));

        Drawable drawable = AppCompatResources.getDrawable(this,R.drawable.ic_baseline_alt_route_24);
        imageView.setImageDrawable(drawable);

        helloWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecondActivity.this,DemoGoogleMap.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        Log.d(TAG , "onStart");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d(TAG , "onStop");
        super.onStop();
    }

    @Override
    protected void onResume() {
        Log.d(TAG , "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG , "onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG , "onDestroy");
        super.onDestroy();
    }
}
