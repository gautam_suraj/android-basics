package com.texas.myapplication;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class VollyActivity extends AppCompatActivity {
    /*
    * Volley is a HTTP library developed by Google and was first introduced during Google I/O 2013.
    * This library is used to transmit data over the network. It actually makes networking faster and easier for Apps.
    *  It is available through AOSP(Android Open Source Project) repository.
    *
    *
    * The volley library has the features like automatic scheduling of network request,
    *  multiple concurrent connections, request prioritization, cancel/block a request,
    * easier management of UI with data fetched asynchronously
    * from the network and also offers easier customization
    *
    *
    * Volley uses cache to improve the App performance by saving memory and bandwidth of remote server.
    *
    * Volley uses caches concept to improve the performance of App.
    * For example, lets say you are using Asynctask to fetch image and description
    * from a JSON array created in server API.
    * The content is fetched in the portrait mode and
    *  now user rotate screen to change it to landscape mode.
    * The activity is destroyed and so the content will be fetched again.
    * Since the user is calling the same resource again to server
    * so it is a waste of resource and ultimately a poor user experience.

       Volley provide solution to this problem as it caches the data.
       * When user request the same data, instead of calling from server
       * Volley directly shows it from cache saving resource and thus improving user experience.
    *
    * */


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    private void fetch() {

        /*
        * RequestQueue and mainly used for smaller Networking purposes in Android.
        *
        * */

        //RequestQueue initialized
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        String url = "https://jsonplaceholder.typicode.com/todos/1";

        //String Request initialized
        StringRequest mStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getApplicationContext(), "Response :"
                        + response.toString(), Toast.LENGTH_LONG).show();//display the response on screen

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        mRequestQueue.add(mStringRequest);
    }
}
