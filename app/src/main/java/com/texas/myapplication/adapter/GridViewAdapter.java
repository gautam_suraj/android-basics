package com.texas.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.texas.myapplication.R;
import com.texas.myapplication.model.ArrayItem;
import com.texas.myapplication.model.GridItem;

import java.util.List;

public class GridViewAdapter extends ArrayAdapter<GridItem> {

    private final Context mContext;

    public GridViewAdapter(@NonNull Context context, @NonNull List<GridItem> items) {
        super(context, R.layout.layout_grid_item, items);
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.layout_grid_item,null);
        }

        GridItem item = getItem(position);

        if (item != null) {
            ImageView imageView = (ImageView) v.findViewById(R.id.iv1);
            TextView subTitle = (TextView) v.findViewById(R.id.tv1);

            if (imageView != null) {
                //imageView.setImageDrawable(imageView.getContext().getDrawable(R.drawable.d1));
                //imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.getContext(),R.drawable.flower));
                imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.getContext(),item.getImage()));
            }

            if (subTitle != null) {
                subTitle.setText(item.getName());
            }
        }

        return v;
    }

    static class GridViewHolder extends RecyclerView.ViewHolder{

        public GridViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
