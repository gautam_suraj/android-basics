package com.texas.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.texas.myapplication.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecycleViewHolder> {

    private List<String> items;

    public RecyclerViewAdapter(List<String> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.layout_simple_string_view, parent, false);
        return new RecycleViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, int position) {
        String text = items.get(position);
        holder.bindView(text);

        //holder.textView.setText(text);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    static class RecycleViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = textView.findViewById(R.id.text);
        }

        public void bindView(String text){
            textView.setText(text);
        }
    }
}
