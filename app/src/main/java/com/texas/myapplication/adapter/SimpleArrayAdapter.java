package com.texas.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.texas.myapplication.R;
import com.texas.myapplication.model.ArrayItem;

import java.util.List;

public class SimpleArrayAdapter extends ArrayAdapter<ArrayItem> {
    private final int resourceLayout;
    private final Context mContext;

    public SimpleArrayAdapter(@NonNull Context context, int resource, @NonNull List<ArrayItem> items) {
        super(context, resource, items);
        this.mContext = context;
        this.resourceLayout = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
           v = LayoutInflater.from(mContext).inflate(resourceLayout,null);
        }

        ArrayItem item = getItem(position);

        if (item != null) {
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView subTitle = (TextView) v.findViewById(R.id.sub_title);

            if (title != null) {
                title.setText(item.getTitle());
            }

            if (subTitle != null) {
                subTitle.setText(item.getSubTitle());
            }
        }

        return v;
    }

    @Nullable
    @Override
    public ArrayItem getItem(int position) {
        return super.getItem(position);
    }
}
