package com.texas.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.texas.myapplication.GridViewActivity;
import com.texas.myapplication.ListViewActivity;
import com.texas.myapplication.R;
import com.texas.myapplication.RecyclerViewActivity;

public class Fragment2 extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_2,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button button = view.findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(view.getContext(), ListViewActivity.class));
                //startActivity(new Intent(view.getContext(), GridViewActivity.class));
                startActivity(new Intent(view.getContext(), RecyclerViewActivity.class));
            }
        });
    }
}
