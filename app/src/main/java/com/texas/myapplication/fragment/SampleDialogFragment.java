package com.texas.myapplication.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.texas.myapplication.R;
import com.texas.myapplication.listener.DialogCallBackListener;

public class SampleDialogFragment extends DialogFragment {
    private DialogCallBackListener listener;

    public static final String TAG = SampleDialogFragment.class.getSimpleName();




    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
       return new Dialog(getActivity(), R.style.DialogTheme){
           @Override
           public void onBackPressed() {
              dismiss();
           }
       };
    }



    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        if(activity instanceof DialogCallBackListener){
           listener = (DialogCallBackListener) activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sample_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView textView = view.findViewById(R.id.text);
        Button button = view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*

               MultipleFragmentActivity activity =  (MultipleFragmentActivity) getActivity();
               activity.onDialogButtonClicked();

               */

                listener.onDialogButtonClicked();


            }
        });
    }

}
