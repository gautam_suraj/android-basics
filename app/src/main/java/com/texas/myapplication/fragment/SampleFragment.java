package com.texas.myapplication.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.texas.myapplication.R;
import com.texas.myapplication.model.Student;

public class SampleFragment extends Fragment {

    public static final String TAG = SampleFragment.class.getSimpleName();
    private Student student;
    /*
    * A Fragment is a piece of an activity which enable more modular activity design.
    *  It will not be wrong if we say, a fragment is a kind of sub-activity.
    *
    * */


    /*
    * A fragment has its own layout and its own behaviour with its own life cycle callbacks.

    You can add or remove fragments in an activity while the activity is running.

    You can combine multiple fragments in a single activity to build a multi-pane UI.

    A fragment can be used in multiple activities.

    Fragment life cycle is closely related to the life cycle of its host activity which means when the activity is paused, all the fragments available in the activity will also be stopped.

    A fragment can implement a behaviour that has no user interface component.

    Fragments were added to the Android API in Honeycomb version of Android which API version 11.
    *
    *
    * */


    /*
    * Prior to fragment introduction,
    *  we had a limitation because we can show only a single activity on the screen at one given point in time.
     *
    * */


    /*
    * But with the introduction of fragment we got more flexibility and removed
    * the limitation of having a single activity on the screen at a time.
    *
    * */

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach: Activity");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");

        /*
        * The fragment instance is associated with an activity instance.
        * The fragment and the activity is not fully initialized.
        *
        *
        * */
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");

        /*
        *  initialize essential components of the fragment that you want to retain when the fragment is paused or stopped, then resumed.
        *
        * */

       Bundle bundle =  getArguments();
       student = (Student) bundle.getSerializable("student");
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        return inflater.inflate(R.layout.fragment_sample,container,false);
       

        /*
        * The system calls this callback when it's time for the fragment to draw its user interface for the first time.
        * */


        /*
        * return null if the fragment does not provide a UI.
        * */
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: ");

        /*
        * when the host activity is created
        *
        * Activity and fragment instance have been created as well as the view hierarchy of the activity.
        *
        * view can be accessed with the findViewById()
        * */
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");

        TextView name = view.findViewById(R.id.name);
        TextView address = view.findViewById(R.id.address);
        TextView semester  = view.findViewById(R.id.semester);
        TextView subjects = view.findViewById(R.id.subjects);

        name.setText(student.getName());
        address.setText(student.getAddress());
        semester.setText(student.getSemester());
        subjects.setText(student.getIntrestedSubject().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");

        /*
        * fragment gets visible.
        * */
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");

        /*
        * Fragment becomes active.
        * */
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
        /*
        *   user is leaving the fragment.
        *
        *    commit changes that should be persisted(stored) beyond the current user session
        * */
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();

        /*
        * Fragment going to be stopped by calling onStop()
        * */
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView: ");

        /*
        * Fragment view will destroy
         * */
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        /*
        * clean up of the fragment's state
        * */
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach: ");
        super.onDetach();
    }
}

