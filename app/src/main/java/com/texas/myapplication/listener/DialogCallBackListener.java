package com.texas.myapplication.listener;

public interface DialogCallBackListener {

    void onDialogButtonClicked();
}
