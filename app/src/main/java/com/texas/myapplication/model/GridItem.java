package com.texas.myapplication.model;

public class GridItem {
    private String name;
    private int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public GridItem(String name, int image) {
        this.name = name;
        this.image = image;
    }
}
