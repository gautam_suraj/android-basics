package com.texas.myapplication.model;

import java.io.Serializable;
import java.util.List;

public class Student implements Serializable {
    private int id;
    private String name;
    private String address;
    private String semester;
    private List<String> intrestedSubject;

    public Student(int id, String name, String address, String semester) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.semester = semester;
    }

    public Student(String name, String address, String semester) {
        this.name = name;
        this.address = address;
        this.semester = semester;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public List<String> getIntrestedSubject() {
        return intrestedSubject;
    }

    public void setIntrestedSubject(List<String> intrestedSubject) {
        this.intrestedSubject = intrestedSubject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", semester='" + semester + '\'' +
                ", intrestedSubject=" + intrestedSubject +
                '}';
    }
}
